
import shutil
import os
import time

import time
import os
import shutil
import redis
import pickle
from pymongo import MongoClient
import time
from datetime import datetime
import random
from bson import ObjectId
import time

MONGO_DB = "DEMO"
INSPECTION_COLLECTION = "inspection"
MONGO_COLLECTION_PARTS = "parts"
MONGO_COLLECTIONS = {MONGO_COLLECTION_PARTS: "parts"}

def singleton(cls):
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance

@singleton
class CacheHelper():
    def __init__(self):
        REDIS_CLIENT_HOST = "localhost"
        REDIS_CLIENT_PORT = 6379
        self.redis_cache = redis.StrictRedis(host=REDIS_CLIENT_HOST, port=REDIS_CLIENT_PORT, db=0, socket_timeout=1)
        REDIS_CLIENT_HOST
        print("REDIS CACHE UP!")

    def get_redis_pipeline(self):
        return self.redis_cache.pipeline()
    
    def set_json(self, dict_obj):
        try:
            k, v = list(dict_obj.items())[0]
            v = pickle.dumps(v)
            return self.redis_cache.set(k, v)
        except redis.ConnectionError:
            return None

    def get_json(self, key):
        try:
            temp = self.redis_cache.get(key)
            if temp:
                try:
                    temp= pickle.loads(temp)
                except:
                    temp = json.loads(temp.decode())
            return temp
        except redis.ConnectionError:
            return None
        return None

    def execute_pipe_commands(self, commands):
        return None

@singleton
class MongoHelper:
    client = None
    def __init__(self):
        if not self.client:
            self.client = MongoClient(host='localhost', port=27017)
        self.db = self.client[MONGO_DB]

    def getDatabase(self):
        return self.db

    def getCollection(self, cname, create=False, codec_options=None):
        _DB = MONGO_DB
        DB = self.client[_DB]
        if cname in MONGO_COLLECTIONS:
            if codec_options:
                return DB.get_collection(MONGO_COLLECTIONS[cname], codec_options=codec_options)
            return DB[MONGO_COLLECTIONS[cname]]
        else:
            return DB[cname]
# Source and destination folders
source_folder = 'D:/Hemlock-U-Rod/HEMLCOK_U_ROD/copy_path/'
destination_folder = "D:/Hemlock-U-Rod/HEMLCOK_U_ROD/source_path/"
redis_server = CacheHelper()
# File to be copied
file_to_copy = 'fuse.pts'
count = 0
# Loop indefinitely

while True:
    print(count)
    if redis_server.get_json("inspection_trigger"):
        if count == 52:
            break
        elif len(os.listdir(destination_folder)) == 0:
            shutil.copy(os.path.join(source_folder, file_to_copy), os.path.join(destination_folder, file_to_copy))
            count = count + 1
        else:
            pass    
    else:
        pass    

from common.utils import CacheHelper
from django.http import response
import inspection
import numpy as np
import cv2 
from numpy import append, array
import json
import base64
import multiprocessing
import sys
from pymongo import MongoClient
from bson import ObjectId
from livis import settings as settings
from livis.settings import BASE_URL
import os
import time
import datetime
from common.utils import *
import os
import shutil
import time
import datetime
from bson.objectid import ObjectId
import random
import re

class Process:
    def __init__(self):
        self.redis_server = CacheHelper()
        self.mongo_server = MongoHelper()
        
    def get_scan_details_util(self, data):
        try:
            scan_collection = self.mongo_server.getCollection('scan_details')
            scan_doc = scan_collection.find_one(sort=[('_id', -1)])
            if scan_doc:
                scan_details_id = scan_doc['_id']
            else:
                scan_details_id = None
            start_num = 0
            end_num = 9999
            generate_no = [str(num).zfill(5) for num in range(start_num, end_num + 1)]
            index = generate_no.index(scan_doc['scan_id'])
            scan_id = generate_no[index + 1]
            reactor_integer = int(scan_doc['reactor_no']) +  1
            batch_no = int(scan_doc['batch_no']) +  1
            P5 = scan_doc['P5']
            prefix, number_str = P5.rsplit(' ', 1)
            new_number = int(number_str) + 1
            P5_details = f'{prefix} {new_number}'
            R3 = scan_doc['R3']
            prefix, number_str = R3.rsplit(' ', 1)
            new_number = int(number_str) + 1
            R3_details = f'{prefix} {new_number}'
            start_num = 0
            end_num = 9999
            generate_no = [str(num).zfill(10) for num in range(start_num, end_num + 1)]
            index = generate_no.index(scan_doc['batch_details'])
            batch_details = generate_no[index + 1]
            pack_no = P5_details + "_" + R3_details
            R3_details = R3_details.split()  
            R3_details = R3_details[-1]
            P5_details = P5_details.split()  
            P5_details = P5_details[-1]
            scan_details_doc = {
                    "scan_id": scan_id,
                    "reactor_no": reactor_integer,
                    "batch_no": batch_no,
                    "pack_no": pack_no,
                    "P": P5_details,
                    "R": R3_details,
                    "batch_details": batch_details
                    }
            if scan_details_id:
                if scan_doc is None:
                    scan_collection.insert(scan_details_doc)
                else:
                    scan_collection.update_one({'_id': ObjectId(scan_details_id)}, {'$set': scan_details_doc})
            return scan_details_doc,200        
        except Exception as scan_details_doc:
            return scan_details_doc,403                    

    def start_process_util(self, data):
        scan_collection = self.mongo_server.getCollection('scan_details')
        scan_doc = scan_collection.find_one(sort=[('_id', -1)])
        if scan_doc:
            scan_id = scan_doc['scan_id']
            batch_details = scan_doc['batch_details']
        else:
            scan_id = None
            batch_details = None
        reactor_no = data.get('reactor_no', None)
        batch_no = data.get('batch_no', None)
        Pack_details = data.get('pack_no', None)
        split_pack = Pack_details.split("_")
        P5_details = split_pack[0]
        P = P5_details.split()  
        P = P[-1]
        R3_details = split_pack[1]
        R = R3_details.split()  
        R = R[-1]

        self.redis_server.set_json({'scan_id': scan_id})
        self.redis_server.set_json({"reactor_no": reactor_no})
        self.redis_server.set_json({"batch_no": batch_no})
        self.redis_server.set_json({"pack_no": Pack_details})
        self.redis_server.set_json({"P": P})
        self.redis_server.set_json({"R": R})
        self.redis_server.set_json({"batch_details": batch_details})        
        # if part_name is None:
        #     return "part name not present", 200

        mp = self.mongo_server.getCollection('inspection', None)
        current_date = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        createdAt = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        coll = {
            "part_name": 'U_ROD',
            "start_time": createdAt,
            "end_time": "",
            "scan_id": scan_id,
            "reactor_no": reactor_no,
            "batch_no": batch_no,
            "P5_details": P5_details,
            "R3_details": R3_details,
            "batch_details": batch_details,
        }
        curr_insp_id = mp.insert_one(coll)
        ret = mp.find_one({'_id': curr_insp_id.inserted_id})
        response = {"_id": ret["_id"]}
        bb = self.mongo_server.getCollection('current_inspection')
        ps = bb.find_one()
        today = datetime.datetime.now().date()
        datetime_format = "%Y-%m-%d %H:%M:%S"
        converted_datetime = datetime.datetime.strptime(ps['time_stamp'], datetime_format)
        converted_datetime = converted_datetime.date()
        if converted_datetime != today:
            if ps is None:
                bb.insert_one({'current_inspection_id': str(curr_insp_id.inserted_id) , 'time_stamp':createdAt})
            else:
                ps['current_inspection_id'] = str(curr_insp_id.inserted_id)
                ps['time_stamp'] = createdAt
                bb.update_one({"_id": ps['_id']}, {"$set": ps})
                response = {"current_inspection_id": curr_insp_id.inserted_id,"part_name":"U-ROD"}
        else:
            current_inspection_id = ps['current_inspection_id']  
            current_inspection_id = ObjectId(current_inspection_id)
            response = {"current_inspection_id": current_inspection_id,"part_name":"U-ROD"}
        self.redis_server.set_json({"inspection_trigger":True})
        self.redis_server.set_json({"inspection_process":True})
        self.redis_server.set_json({"scan_trigger":False})
        self.redis_server.set_json({"is_scan_complete":False})
        return response, 200
    
    def get_data_feed(self, topic):
        b = self.redis_server.get_json(topic)
        result = b
        return result

    def get_single_frame(self, topic):
        frame = self.redis_server.get_json(topic)
        return frame

    def create_urls_from_camera(self, camera_id, BASE_URL):
        fmt_url = BASE_URL + '/livis/v1/inspection/get_output_stream/{}/'
        return fmt_url.format(camera_id)

    def get_running_process_utils(self):
        mp = self.mongo_helper.getCollection('inspection')
        insp_coll = [i for i in mp.find({"status": "started"})]
        inspection_id = ""
        response = {}
        if len(insp_coll) > 0:
            res = insp_coll[-1]
            response["inspection_id"] = str(res['_id'])
            response["part_name"] = res["part_name"]
        return response, 200
    
    def end_process_util(self, data):
        self.redis_server.set_json({'scan_id': None})
        self.redis_server.set_json({"reactor_no": None})
        self.redis_server.set_json({"batch_no": None})
        self.redis_server.set_json({"P5_details": None})
        self.redis_server.set_json({"R3_details": None})
        self.redis_server.set_json({"batch_details": None})
        self.redis_server.set_json({"inspection_trigger":False})
        self.redis_server.set_json({"inspection_process":False})
        mp = self.mongo_server.getCollection('current_inspection')
        doc = mp.find_one()
        if doc:
            inspection_id = doc["current_inspection_id"]

        endedAt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        colle = {
            "status": "completed",
            "end_time": endedAt
        }
        bb = self.mongo_server.getCollection('current_inspection')
        ps = bb.find_one()
        return {}, 200

class GetMetrics:
    def __init__(self):
        self.redis_server = CacheHelper()
        self.mongo_server = MongoHelper()
        self.current_inspection_collection = self.mongo_server.getCollection('current_inspection')
        self.current_inspection_id  = self.current_inspection_collection.find_one()
        self.source_path = 'D:/Hemlock-U-Rod/HEMLCOK_U_ROD/source_path/'
        self.dest_path = 'D:/Hemlock-U-Rod/HEMLCOK_U_ROD/dest_path/'
        self.metrics = self.inspection_metrics()
    
    
    def get_complete_inspection_details(self):
        collection_name = 'current_inspection'
        current_inspection_collection = self.mongo_server.getCollection(collection_name)
        current_inspection_doc = current_inspection_collection.find_one()
        if current_inspection_doc:
            current_inspection_id = current_inspection_doc['current_inspection_id']
            collection_name = f"{current_inspection_id}_results"
            mp = self.mongo_server.getCollection(collection_name)
            latest_doc = mp.find_one(sort=[('_id', -1)])
            if latest_doc:
                return latest_doc['complete_inspection_details']
        return None
    
    def get_analytics(self):
        try:
            complete_inspection_details = self.get_complete_inspection_details()
            if complete_inspection_details:
                indicators_count = {}
                individual_count = {}
                total_inspection_count = len(complete_inspection_details)
                for item in complete_inspection_details:
                    indicators_status = item['indicators_status']
                    indicators_count[indicators_status] = indicators_count.get(indicators_status, 0) + 1
                return indicators_count
            else:
                return None
        except Exception as e:
            return None
        
    def inspection_metrics(self):
        try:
            previous_inspection_id = None
            _id_new = None
            is_process_started =  False
            inspection_id = self.current_inspection_id['current_inspection_id']
            previous_inspection_id = inspection_id
            mp = self.mongo_server.getCollection(str(inspection_id) + '_results')
            dataset = [p for p in mp.find().sort("$natural", -1)]
            dataset1 = [p for p in mp.find({"status": "Accepted"})]
            dataset2 = [p for p in mp.find({"status": "Rejected"})]
            total_production = len(dataset)
            total_accepted_count = len(dataset1)
            total_rejected_count = len(dataset2)
            latest_doc = mp.find_one(sort=[('_id', -1)])
            inspection_metrics_obj = {}
            scan_id = self.redis_server.get_json('scan_id')
            reactor_no = self.redis_server.get_json("reactor_no")
            batch_no = self.redis_server.get_json("batch_no")
            pack_no = self.redis_server.get_json("pack_no")
            P5_details = self.redis_server.get_json("P")
            R3_details = self.redis_server.get_json("R")
            batch_details = self.redis_server.get_json("batch_details")
            scan_details = {
                "scan_id": scan_id,
                "reactor_no": reactor_no,
                "batch_no": batch_no,
                "pack_no": pack_no,
                "P": P5_details,
                "R": R3_details,
                "batch_details": batch_details
                }
            if latest_doc:
                data = latest_doc['complete_inspection_details']
            else:
                data = None
            if self.current_inspection_id['current_inspection_id'] is None :
                is_process_started = False
            else:
                is_process_started = True  
            if data:
                del dataset[0]['complete_inspection_details']
                inspection_metrics_obj['scan_trigger'] = self.redis_server.get_json("scan_trigger")
                inspection_metrics_obj['scan_details'] = scan_details
                inspection_metrics_obj['individual_inspection'] = data
                inspection_metrics_obj['total'] = str(total_production)
                inspection_metrics_obj['total_accepted'] = str(total_accepted_count)
                inspection_metrics_obj['total_rejected'] = str(total_rejected_count)
                inspection_metrics_obj['indicators_count'] = self.get_analytics()
                inspection_metrics_obj['is_processing'] = self.redis_server.get_json("inspection_process")
                inspection_metrics_obj['is_process_started'] = is_process_started
                inspection_metrics_obj['is_indivdual_status'] = {"is_status":self.redis_server.get_json("is_indivdual_status") , "slice_count": int(self.redis_server.get_json("total_batch_processed"))}
                inspection_metrics_obj['is_scan'] = self.redis_server.get_json("inspection_rescan")
                inspection_metrics_obj['is_scan_completed'] = self.redis_server.get_json("is_scan_complete")
            else:
                inspection_metrics_obj['scan_trigger'] = self.redis_server.get_json("scan_trigger")
                inspection_metrics_obj['scan_details'] = scan_details
                inspection_metrics_obj['individual_inspection'] = data
                inspection_metrics_obj['total'] = str(total_production)
                inspection_metrics_obj['total_accepted'] = str(total_accepted_count)
                inspection_metrics_obj['total_rejected'] = str(total_rejected_count)
                inspection_metrics_obj['indicators_count'] = self.get_analytics()
                inspection_metrics_obj['is_processing'] = self.redis_server.get_json("inspection_process")
                inspection_metrics_obj['is_process_started'] = is_process_started
                inspection_metrics_obj['is_indivdual_status'] = {"is_status":self.redis_server.get_json("is_indivdual_status") , "slice_count": int(self.redis_server.get_json("total_batch_processed"))}
                inspection_metrics_obj['is_scan'] = self.redis_server.get_json("inspection_rescan")
                inspection_metrics_obj['is_scan_completed'] = self.redis_server.get_json("is_scan_complete")          
            inspection_metrics = {
                "inspection_metrics":inspection_metrics_obj,
            }
            return inspection_metrics
        except Exception as e:
            print(e)
            inspection_metrics_obj = {}
            inspection_metrics_obj['scan_trigger'] = self.redis_server.get_json("scan_trigger")
            inspection_metrics_obj['is_scan_completed'] = self.redis_server.get_json("is_scan_complete")
            inspection_metrics = { "inspection_metrics":inspection_metrics_obj}

            return inspection_metrics

    def get_inference_feed(self, cam_id):
        print(cam_id)
        key = str(cam_id)
        while True:
            v = self.redis_server.get_json(key)
            im_b64_str = v
            ret, jpeg = cv2.imencode('.jpg', im_b64_str)
            frame = jpeg.tobytes()

            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
            
            
    def get_results(self):
        collection_name = str(self.current_inspection_id['current_inspection_id']) + "_results"
        mp = self.mongo_server.getCollection(collection_name)
        try:
            latest_doc = mp.find_one(sort=[('_id', -1)])
            if latest_doc:
                _id_new = latest_doc['complete_inspection_details']
            else:
                _id_new = None
            if _id_new:
                indicators_count = {}
                individual_count = {}
                for item in _id_new:
                    indicators_status = item['indicators_status']
                    individual_status = item['individual_status']
                    
                    if indicators_status in indicators_count:
                        indicators_count[indicators_status] += 1
                    else:
                        indicators_count[indicators_status] = 1
                        
                    if individual_status in individual_count:
                        individual_count[individual_status] += 1
                    else:
                        individual_count[individual_status] = 1
                if indicators_count.get('E', 0) > 1 or  indicators_count.get('BE', 0) > 5 or  indicators_count.get('B', 0) > 9:
                    status = 'Rejected'
                else:
                    status = 'Accepted'    
                result_dict = {
                    'status':status,
                }
                return result_dict,200
        except Exception as result_dict:
            return result_dict,403
        
        
class Analytics:
    def __init__(self):
        self.redis_server = CacheHelper()
        self.mongo_server = MongoHelper()
    
    def get_complete_inspection_details(self):
        collection_name = 'current_inspection'
        current_inspection_collection = self.mongo_server.getCollection(collection_name)
        current_inspection_doc = current_inspection_collection.find_one()
        
        if current_inspection_doc:
            current_inspection_id = current_inspection_doc['current_inspection_id']
            collection_name = f"{current_inspection_id}_results"
            mp = self.mongo_server.getCollection(collection_name)
            
            latest_doc = mp.find_one(sort=[('_id', -1)])
            if latest_doc:
                return latest_doc['complete_inspection_details']
        
        return None
    
    def get_analytics_util(self):
        try:
            complete_inspection_details = self.get_complete_inspection_details()

            if complete_inspection_details:
                indicators_count = {}
                individual_count = {}
                total_inspection_count = len(complete_inspection_details)

                for item in complete_inspection_details:
                    indicators_status = item['indicators_status']
                    individual_status = item['individual_status']

                    indicators_count[indicators_status] = indicators_count.get(indicators_status, 0) + 1
                    individual_count[individual_status] = individual_count.get(individual_status, 0) + 1

                indicators_percentage = {
                    status: (count / total_inspection_count) * 100
                    for status, count in indicators_count.items()
                }

                individual_percentage = {
                    status: (count / total_inspection_count) * 100
                    for status, count in individual_count.items()
                }

                analytics_dict = {
                    'indicators_count': indicators_count,
                    'indicators_percentage': indicators_percentage,
                    'individual_count': individual_count,
                    'individual_percentage': individual_percentage,
                }

                return analytics_dict, 200
            else:
                return {"message": "No inspection details found"}, 404

        except Exception as e:
            return {"error": str(e)}, 500

                
        
    





from django.contrib import admin
from django.urls import path, re_path
from django.urls import path,re_path
from django.conf.urls import url
from inspection import views

urlpatterns = [
    re_path(r'^get_capture_feed_url/$', views.get_camera_urls),
    re_path(r'^save_results_per_view/$', views.save_inspection_per_view),
    re_path(r'^get_running_process/$', views.get_running_process_views),
    re_path(r'^start_process/$', views.start_process),
    re_path(r'^end_process/$', views.end_process),
    ###################
    re_path(r'^save_inspection_details/$', views.save_inspection_details_view),
    re_path(r'^get_metrics/$', views.get_metrics_view),
    re_path(r'^get_ui_trigger/$', views.start_inspection_view),
    re_path(r'^get_analytics/$', views.get_analytics_view),
    re_path(r'^get_results/$', views.get_results_view),
    re_path(r'^get_scan_details/$', views.get_scan_details_view),

]



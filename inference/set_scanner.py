
import shutil
import os
import time

import time
import os
import shutil
import redis
import pickle
from pymongo import MongoClient
import time
from datetime import datetime
import random
from bson import ObjectId
import time

MONGO_DB = "DEMO"
INSPECTION_COLLECTION = "inspection"
MONGO_COLLECTION_PARTS = "parts"
MONGO_COLLECTIONS = {MONGO_COLLECTION_PARTS: "parts"}

def singleton(cls):
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance

@singleton
class CacheHelper():
    def __init__(self):
        REDIS_CLIENT_HOST = "localhost"
        REDIS_CLIENT_PORT = 6379
        self.redis_cache = redis.StrictRedis(host=REDIS_CLIENT_HOST, port=REDIS_CLIENT_PORT, db=0, socket_timeout=1)
        REDIS_CLIENT_HOST
        print("REDIS CACHE UP!")

    def get_redis_pipeline(self):
        return self.redis_cache.pipeline()
    
    def set_json(self, dict_obj):
        try:
            k, v = list(dict_obj.items())[0]
            v = pickle.dumps(v)
            return self.redis_cache.set(k, v)
        except redis.ConnectionError:
            return None

    def get_json(self, key):
        try:
            temp = self.redis_cache.get(key)
            if temp:
                try:
                    temp= pickle.loads(temp)
                except:
                    temp = json.loads(temp.decode())
            return temp
        except redis.ConnectionError:
            return None
        return None

    def execute_pipe_commands(self, commands):
        return None

@singleton
class MongoHelper:
    client = None
    def __init__(self):
        if not self.client:
            self.client = MongoClient(host='localhost', port=27017)
        self.db = self.client[MONGO_DB]

    def getDatabase(self):
        return self.db

    def getCollection(self, cname, create=False, codec_options=None):
        _DB = MONGO_DB
        DB = self.client[_DB]
        if cname in MONGO_COLLECTIONS:
            if codec_options:
                return DB.get_collection(MONGO_COLLECTIONS[cname], codec_options=codec_options)
            return DB[MONGO_COLLECTIONS[cname]]
        else:
            return DB[cname]

redis_server = CacheHelper()
# File to be copied
file_to_copy = 'fuse.pts'
count = 0
# Loop indefinitely
trigger = input('Enter trigger:' )
if trigger == '1':
    redis_server.set_json({"scan_trigger":True})
elif trigger == '2':
    redis_server.set_json({"scan_trigger":False})    
elif trigger == '3':
    redis_server.set_json({"inspection_rescan":False})
elif trigger == '4':
    redis_server.set_json({"inspection_rescan":True})
elif trigger == '5':
    redis_server.set_json({"inspection_trigger":True})
elif trigger == '6':
    redis_server.set_json({"inspection_trigger":False})
elif trigger == '7':
    redis_server.set_json({"is_scan_complete":True})
elif trigger == '8':
    redis_server.set_json({"is_scan_complete":False})

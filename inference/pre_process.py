# Imports
import pandas as pd
import pickle
import numpy as np
import open3d as o3d
import copy
import matplotlib.pyplot as plt
import os
import time

"""
This class pre-process the input point cloud by removing outliers, computing the geometric features
required for training the model and removing any selecting only the features required to do inferencing
from the pre-trained model.
"""
class PreprocessPCD():
    def __init__(self, source_file):
        self.data_path = source_file

    # Build point cloud from dataframe
    def load_pc_points(self, df):
        points = np.array(df[['X', 'Y', 'Z']])
        pcd = o3d.geometry.PointCloud()
        pcd.points = o3d.utility.Vector3dVector(points)
        return pcd
    
    # Move point cloud to the origin
    def move_to_centre(self, df_original):
        df = df_original.copy()
        pcd = self.load_pc_points(df)
        pcd.translate(-pcd.get_center())
        pcd_points = np.asarray(pcd.points)
        df[['X', 'Y', 'Z']] = pcd_points
        df = df.reset_index(drop=True)
        return df
    
    # Check for null values in the dataframe
    def check_null_rows(self, df):
        nan_mask = df.isna().any(axis=1)
        rows_with_nan = df[nan_mask]
        non_repetitive_rows = rows_with_nan.drop_duplicates()
        count_non_repetitive_rows_with_nan = len(non_repetitive_rows)
        return count_non_repetitive_rows_with_nan

    # Pre-process the data
    def check_data(self):
        df_test = None
        try:
            df = pd.read_csv(self.data_path, delimiter=" ", header=0)
            df_origin = self.move_to_centre(df)
            null_values = self.check_null_rows(df_origin)
            if null_values:
                df_origin.fillna(df_origin.mean(), inplace=True)
            df_test = df_origin.drop(['X', 'Y','Classification', 'Verticality'], axis=1)
        except Exception as e:
            print(e)

        return df_test

"""
This class is used to do Inferencing of the input point cloud data and give the ranking of the surfaces.
The output classes are E(for rough surface) and A(for smooth surface).
"""
class InferencingPCD():
    def __init__(self, source_file, model):
        self.inference_file = source_file
        self.loaded_classifier = model
    # Plot histogram for percentage prediction of each class
    def save_class_histogram(self, percentages):
        class_labels = {1: "E", 2: "A"}
        # Extract the percentages and labels
        percentage_list = list(percentages.values())
        labels = [class_labels[label] for label in percentages.keys()]
        # Set up Matplotlib to use the 'agg' backend for off-screen rendering
        plt.switch_backend('agg')
        # Create the histogram
        plt.bar(labels, percentage_list)
        # Add percentage numbers on top of each bar
        for i, percentage in enumerate(percentage_list):
            plt.text(i, percentage, f"{percentage}%", ha='center', va='bottom')
        # Set labels and title
        plt.xlabel("Label")
        plt.ylabel("Label Predictions (in %)")
        plt.title("Prediction Percentages Histogram")
        # Render the figure as a NumPy array
        fig = plt.gcf()
        fig.canvas.draw()
        image_data = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
        image_data = image_data.reshape(fig.canvas.get_width_height()[::-1] + (3,))
        return image_data

    # Post process the predicted labels
    def post_processing(self, predicted_labels):
        predicted_labels_int = np.array(predicted_labels, dtype=np.int64)
        class_counts = {}
        for label in predicted_labels_int:
            class_counts[label] = class_counts.get(label, 0) + 1
        percentages = {label: round(count/len(predicted_labels_int)*100, 2) for label, count in class_counts.items()}
        img_data = self.save_class_histogram(percentages)
        max_class = max(percentages, key=percentages.get)
        return max_class, img_data
        
    # Do inferencing of the input pcd
    def infer_pcd(self):
        indicators_status = None
        try:
            preprocessor = PreprocessPCD(self.inference_file)
            df_preprocessed = preprocessor.check_data()
            predicted_labels = self.loaded_classifier.predict(df_preprocessed)
            max_class, label_plot = self.post_processing(predicted_labels)
            if max_class == '2':
                indicators_status = 'A'
            else:
                indicators_status = 'E'
                
            return indicators_status, label_plot
        except Exception as e:
            print(e)


if __name__ == '__main__':
    source_file = "./validation_23_2.txt"
    model_file = "./knn_classifier_45perc.pkl"
    inferencer = InferencingPCD(source_file, model_file)
    start_time = time.time()
    indicator_status, label_plot = inferencer.infer_pcd()
    end_time =  time.time() - start_time
    print(f"Inferencing takes {end_time} s")

    
import time
import os
import shutil
import redis
import pickle
from pymongo import MongoClient
import time
from datetime import datetime
import random
from bson import ObjectId
import time
from pre_process import *
import bson
import cv2
import traceback
import sys

MONGO_DB = "DEMO"
INSPECTION_COLLECTION = "inspection"
MONGO_COLLECTION_PARTS = "parts"
MONGO_COLLECTIONS = {MONGO_COLLECTION_PARTS: "parts"}

def singleton(cls):
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance

@singleton
class CacheHelper():
    def __init__(self):
        REDIS_CLIENT_HOST = "localhost"
        REDIS_CLIENT_PORT = 6379
        self.redis_cache = redis.StrictRedis(host=REDIS_CLIENT_HOST, port=REDIS_CLIENT_PORT, db=0, socket_timeout=1)
        REDIS_CLIENT_HOST
        print("REDIS CACHE UP!")

    def get_redis_pipeline(self):
        return self.redis_cache.pipeline()
    
    def set_json(self, dict_obj):
        try:
            k, v = list(dict_obj.items())[0]
            v = pickle.dumps(v)
            return self.redis_cache.set(k, v)
        except redis.ConnectionError:
            return None

    def get_json(self, key):
        try:
            temp = self.redis_cache.get(key)
            if temp:
                try:
                    temp= pickle.loads(temp)
                except:
                    temp = json.loads(temp.decode())
            return temp
        except redis.ConnectionError:
            return None
        return None

    def execute_pipe_commands(self, commands):
        return None

@singleton
class MongoHelper:
    client = None
    def __init__(self):
        if not self.client:
            self.client = MongoClient(host='localhost', port=27017)
        self.db = self.client[MONGO_DB]

    def getDatabase(self):
        return self.db

    def getCollection(self, cname, create=False, codec_options=None):
        _DB = MONGO_DB
        DB = self.client[_DB]
        if cname in MONGO_COLLECTIONS:
            if codec_options:
                return DB.get_collection(MONGO_COLLECTIONS[cname], codec_options=codec_options)
            return DB[MONGO_COLLECTIONS[cname]]
        else:
            return DB[cname]


class AI_Process:
    def __init__(self):
        self.redis_server = CacheHelper()
        self.mongo_server = MongoHelper()
        self.path = os.getcwd()
        self.path = os.path.normpath(self.path)  
        self.path = self.path.replace("\\", "/")
        self.source_path = self.path + "/infrerence_file/"

    def save_individual_inspection_details_util(self,inspection_count,indicator_status,distribution_plot_path):
        individual_status = None
        # indicators_status = random.choice(["AB", "A" ,"BE", "E" ,"B"])
        if indicator_status == 'A':
            individual_status = 'Good'
            hex_color = '#E7F8B4'
        else:
            individual_status = 'Bad'
            hex_color = '#FAE5E5'
        inspection_details = {
        "slice_position": inspection_count,
        "individual_status": individual_status,
        "indicators_status": indicator_status,
        "individual_distribution_plot" : distribution_plot_path,
        "individual_insp_created_at": datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        "individual_time_stamp": str(datetime.now().replace(microsecond=0)),
        "hex_color":hex_color,
        "individual_remark": "",
        }
        current_inspection_collection = self.mongo_server.getCollection('current_inspection')
        current_inspection_id  = current_inspection_collection.find_one()
        inspection_id = current_inspection_id['current_inspection_id']
        collection_name = str(inspection_id) + "_results"
        mp = self.mongo_server.getCollection(collection_name)
        latest_doc = mp.find_one(sort=[('_id', -1)])
        if latest_doc:
            _id_new = latest_doc['_id']
        else:
            _id_new = None

        if _id_new:
            existing_inspection_count = latest_doc.get('inspection_details', {}).get('inspection_count')
            if existing_inspection_count is None or existing_inspection_count != inspection_details['inspection_count']:
                mp.update({'_id' :ObjectId(_id_new)}, {'$push': {'complete_inspection_details': {'$each': [inspection_details]}}})
        else:
            pass
        if inspection_count == 52:
            _id_new = latest_doc['_id']
            status = {"status": 'Accepted',}
            mp.update({'_id' :ObjectId(_id_new)}, {"$set" : status})
        return inspection_details 

    def save_inspection_details_util(self):
        previous_inspection_id = None
        current_inspection_collection = self.mongo_server.getCollection('current_inspection')
        current_inspection_id  = current_inspection_collection.find_one()
        inspection_id = current_inspection_id['current_inspection_id']
        operator_name = self.redis_server.get_json("operator_name")
        user_role = self.redis_server.get_json("user_role")
        operator_id = self.redis_server.get_json("operator_id")
        mp = self.mongo_server.getCollection(str(inspection_id) + "_results")
        created_at = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        inspection_details = {
                "created_at" : created_at,  
                "complete_inspection_details" : [],
                "part_name" : 'U-ROD',
                "operator_name" : 'hemlock-operator',
                "user_role" : 'operator',
                "operator_id" : 1,
                "end_time" : datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"),
                "remark": "",
                "status":None,
            }
        print(inspection_details, 'inspection_details')
        try:
            mp = self.mongo_server.getCollection(str(inspection_id) + "_results")
            _id = mp.insert_one(inspection_details)
            return "success",200
        except Exception as e:
            print(e)
            return e, 403
        
    def start_ai_process(self,model):
        redis_server = CacheHelper()
        num_of_batch = 0
        save_inspection = False
        try:
            while True:
                print(self.redis_server.get_json("inspection_trigger"),"INSPECTION TRIGGER STATUS")
                if self.redis_server.get_json("inspection_trigger"):
                    self.redis_server.set_json({"scan_trigger":False})
                    if save_inspection == False:
                        self.save_inspection_details_util()
                        save_inspection = True
                    files = os.listdir(self.source_path)
                    
                    source_files = [f for f in files if f.lower().endswith(('.jpg', '.jpeg', '.png', '.txt'))]
                    print(num_of_batch)
                    if num_of_batch == 52:
                        num_of_batch = 0
                        save_inspection = False
                        self.redis_server.set_json({"inspection_trigger":False})
                        self.redis_server.set_json({"inspection_process":False})
                        self.redis_server.set_json({"inspection_rescan":True})
                        time.sleep(2)
                    elif source_files:
                        x = bson.ObjectId()
                        self.redis_server.set_json({"is_indivdual_status":True})
                        self.redis_server.set_json({"total_batch_processed": num_of_batch})
                        self.redis_server.set_json({"inspection_process":True})
                        for source_file in source_files:
                            source_file = os.path.join(self.source_path, source_file)
                            start_time = time.time()
                            inferencer = InferencingPCD(source_file, model)
                            indicator_status, label_plot = inferencer.infer_pcd()
                            print(indicator_status)
                            print(label_plot)
                            end_time =  time.time() - start_time
                            print(f"Inferencing takes {end_time} s")
                            cv2.imwrite(datadrive_path+str(x)+'_distribution_plot.jpg',label_plot)
                            distribution_plot_path = 'http://localhost:3306/'+str(x)+'_distribution_plot.jpg'
                            os.remove(source_file)
                            num_of_batch += 1
                            print('Processed Images and starting inspection')
                            self.redis_server.set_json({"is_indivdual_status":False})
                            inspection_details = self.save_individual_inspection_details_util(num_of_batch,indicator_status,distribution_plot_path)
                    else:
                        print('No Images to process waiting for images to be processed')
                        # self.redis_server.set_json({"inspection_trigger":False})
                    time.sleep(2)
        except Exception as e:
            print(f"An exception occurred on line {traceback.extract_tb(sys.exc_info()[2])[-1][1]}: {e}")
            # print(e,"CODE BREAK ERROR")
            self.redis_server.set_json({"inspection_trigger": False})

if __name__ == "__main__":
    ai_processor = AI_Process()
    path = os.getcwd()
    path = os.path.normpath(path)  
    path = path.replace("\\", "/")
    datadrive_path = path + '/data_drive/'
    model_file = "./knn_classifier_45perc.pkl"
    print("MODEL LOADED AND READY TO INSPECT")
    with open(model_file, 'rb') as file:
        model = pickle.load(file)
    ai_processor.start_ai_process(model)



































